import React, {Component, Fragment} from 'react';
import './HomeWork59.css';
import NoteShortDesc from "../../components/Notes/NoteShortDesc/NoteShortDesc";
import axios from 'axios';
import NoteAdd from "../../components/NoteAdd/NoteAdd";


class App extends Component {

    state = {
        films: [
            {name: 'Черная Пантера', id: 1},
            {name: 'Мстители: Война бесконечности', id: 2},
            {name: 'Дэдпул 2', id: 3},
            {name: 'Человек-Муравей и Оса', id: 4}
        ],
    };

    componentDidMount() {
        axios.get('/items.json').then((response) => {
            console.log(response);
            const films = [];

            for(let key in response.data){
                films.push({...response.data[key], id:key })
            }
            this.setState({films})
        });
    }

    render() {
        return (
            <Fragment>
                <div className="HomeWork59">
                    <NoteAdd change={this.componentDidMount}/>
                    <div className='MainFiled'>

                        {this.state.films.map(film => (<NoteShortDesc key={film.id} title={film.name}/>))}
                        {/*<NoteShortDesc items={this.state.films} />*/}
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default App;