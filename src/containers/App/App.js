import React, {Component, Fragment} from 'react';
import {Route, Switch} from 'react-router-dom';
import './App.css';

import HomeWork53 from "../HomeWork53/HomeWork53";
import HomeWork59 from "../HomeWork59/HomeWork59";
import NotFound from "../../components/Page/Error/NotFound/NotFound";
import Navigation from "../../components/Page/Header/Navigation/Navigation";
import HomePage from "../../components/Page/HomePage/HomePage";
import NoteAdd from "../../components/NoteAdd/NoteAdd";
import ItemInfo from "../ItemInfo/ItemInfo";


class App extends Component {

    render() {
        return (
            <Fragment>
                <Navigation/>
                <Switch>
                    <Route path="/" exact component={HomePage}/>
                    <Route path="/add"  component={NoteAdd}/>
                    <Route path="/hw53"  component={HomeWork53}/>
                    <Route path="/hw59"  component={HomeWork59}/>
                    <Route path="/item/:id"  component={ItemInfo}/>
                    <Route render={() => <NotFound style={{textAlign: 'center'}} component={NotFound}/>}/>
                </Switch>
            </Fragment>
        );
    }
}

export default App;
