import React from 'react';
import Task from "./Task/Task";


const MainField = props => {


    return (
        <div className='MainField'>
                {props.items.map(item => {
                    return <Task name={item.name} click={() => props.click(item.id)} key={item.id}/>
                } )}

        </div>
    )
};

export default MainField;