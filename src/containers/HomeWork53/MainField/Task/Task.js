import React from 'react';
import './Task.css';

const Task = props => {
    return (
        <div className='Item'>
            <span className='Name'>{props.name}</span>
            <button className='RemButton' onClick={props.click}>X</button>
        </div>
    )
};

export default Task;