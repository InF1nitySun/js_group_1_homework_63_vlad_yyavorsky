import React, {Component, Fragment} from 'react';
import './HomeWork53.css';
import NoteShortDesc from "../../components/Notes/NoteShortDesc/NoteShortDesc";
import axios from 'axios';


class HomeWork53 extends Component {

    state = {
        items: []
    };

    componentDidMount() {
        axios.get('/items.json').then((response) => {
            const items = [];

            for (let key in response.data) {
                items.push({...response.data[key], id: key})
            }
            this.setState({items})
        });
    }

    ItemDelete = id => {
        axios.delete(`/items/${id}.json`).then(() => {
            this.setState(prevState => {
                const items = [...prevState.items];
                const index = items.findIndex(item => item.id === id);
                items.splice(index, 1);
                return {items};
            })
        })
    };

    render() {
        return (
            <Fragment>
                <div className="HomeWork53">
                    {this.state.items.map(item => (
                        <NoteShortDesc
                            key={item.id}
                            title={item.title}
                            body={item.body}
                            id={item.id}
                            delete={() => this.ItemDelete(item.id)}
                        />
                    ))}
                </div>
            </Fragment>
        );
    }
}

export default HomeWork53;
