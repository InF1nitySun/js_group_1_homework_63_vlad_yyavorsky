import React, {Component, Fragment} from 'react';
import axios from 'axios';
import './ItemInfo.css';
import Loader from "../../components/Loader/Loader";

class ItemInfo extends Component {
    state = {
        loadedItem: null
    };

    componentDidMount() {
        const id = this.props.match.params.id;

        axios.get(`/items/${id}.json`).then((response) => {
            console.log(response.data);
            this.setState({loadedItem: response.data});
        })
    }

    render() {
        if (this.state.loadedItem) {
            return (
                <div className="ItemInfo Container">
                    <h4>{this.state.loadedItem.title}</h4>
                    <p>{this.state.loadedItem.body}</p>
                </div>
            )
        } else {
            return (
                <Loader/>
            )
        }

    }
};

export default ItemInfo;