import React from 'react';
import {NavLink} from 'react-router-dom';
import './Navigation.css';

const Navigation = props => {
    return (
        <div className="Navigation ">
            <nav className="Nav Container">
                <a href="/" title="Домашняя работа 63" alt="home" className="Logos"> </a>
                <ul>
                    <li>
                        <NavLink to="/">HomePage</NavLink>
                    </li>
                    <li>
                        <NavLink to="/add"> add new Notes</NavLink>
                    </li>
                    <li>
                        <NavLink to="/hw53">HomeWork 53</NavLink>
                    </li>
                    <li>
                        <NavLink to="/hw59">HomeWork 59</NavLink>
                    </li>
                    <li>
                        <NavLink to="/qwe">Not Found</NavLink>
                    </li>
                </ul>
            </nav>
        </div>
    )
};

export default Navigation;