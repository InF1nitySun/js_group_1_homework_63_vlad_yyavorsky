import React, {Component} from 'react';
import './NoteAdd.css';
import axios from 'axios';


class NoteAdd extends Component {

    state = {
        item: {
            title: '',
            body: ''
        },
        loading: false
    };

    noteCreateHandler = event => {
        event.preventDefault();
        this.setState({loading: true});

        axios.post('/items.json', this.state.item).then(() => {
            this.setState({loading: false});
            this.props.history.replace('/hw53');
        });
    };

    noteValueChanged = event => {
        event.persist();

        this.setState(prevState =>{
            return {
                item:{...prevState.item, [event.target.name]: event.target.value}
            }
        })
    };

    render() {
        return (
            <form className="NoteAdd Container">
                <input type="text" name="title" placeholder="New note title" value={this.state.item.title}
                       onChange={this.noteValueChanged}/>
                <textarea name="body" placeholder="Enter your notes..." value={this.state.item.body}
                          onChange={this.noteValueChanged}/>
                <button onClick={this.noteCreateHandler}>Add</button>
            </form>
        )
    }


}

export default NoteAdd;