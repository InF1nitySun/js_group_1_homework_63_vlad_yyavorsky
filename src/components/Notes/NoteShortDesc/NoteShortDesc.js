import React from 'react';
import './NoteShortDesc.css';
import {Link} from "react-router-dom";

const NoteShortDesc = props => (
        <div className="NoteShortDesc Container">
            <h4><Link to={'/item/' + props.id}>{props.title}</Link></h4>
            <button onClick={props.delete}>Del</button>
        </div>
);

export default NoteShortDesc;