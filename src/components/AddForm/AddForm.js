import React, {Fragment} from 'react';
import './AddForm.css'


const AddForm = props => {
    return (
        <Fragment>
            <div className='AddForm'>
                <input onChange={props.change} type="text" placeholder='Add New Task'/>
                <button onClick={props.click}>Add</button>
            </div>
        </Fragment>

    )
};

export default AddForm;